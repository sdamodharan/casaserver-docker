FROM ubuntu:14.04

RUN /bin/bash -c 'source $HOME/.bashrc;\
apt-get update;\
apt-get install apache2 php5 php5-dev libapache2-mod-php5 php5-mcrypt php-pear libcurl4-openssl-dev libsasl2-dev libcurl4-openssl-dev pkg-config git -y;'

RUN apt-get install php5-curl php5-gd php5-mysql php5-memcached -y

COPY 000-default.conf /etc/apache2/sites-available/

RUN a2enmod rewrite && a2ensite 000-default

COPY apache2foreground /usr/local/bin/

RUN chmod +X /usr/local/bin/apache2foreground

RUN mkdir /root/.ssh/

ADD id_rsa /root/.ssh/id_rsa

RUN chmod 400 /root/.ssh/id_rsa

# RUN touch /root/.ssh/known_hosts

# RUN ssh-keyscan bitbucket.org >> /root/.ssh/known_hosts

RUN /bin/bash -c 'echo -e "Host bitbucket.org\n    StrictHostKeyChecking no\n" >> /root/.ssh/config;'

WORKDIR /var/www/

RUN git clone git@bitbucket.org:sdamodharan/lb-staging-test.git casalaundry

EXPOSE 80

CMD ["sh","/usr/local/bin/apache2foreground"]